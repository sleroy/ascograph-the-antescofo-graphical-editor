AscoGraph is the new [Antescofo](https://forum.ircam.fr/projects/detail/antescofo) graphical editor.

## Main features ##

- automatic conversion from MusicXML or MIDI scores to Antescofo Language,
- automatic scrolling of notes view and editor view with Antescofo Score Follower (using OpenSoundControl),
- note selection from editor or visual, in both directions for fast and easy modifications : double click on a line in the text editor to see which note it is in the pianoroll,
- graphical editing of curves objects (break point functions),
- syntax coloring, group folding, etc,
- template group (group, loop, curve, whenever, oscsend,…) creation,
- OSC automatic communication with Antescofo (start, play, stop, nextevent, reloadscore on save, etc…),
- simulation and graphical display of performance.

![](https://forum.ircam.fr/media/uploads/software/ascograph.jpg)


Ascograph is built on OpenFrameworks, version 72 and a modified ofxTimeline by James George.
